rust-async-fs (2.1.2-5) unstable; urgency=medium

  * stop mention dh-cargo in long description
  * declare rust-related build-dependencies unconditionally,
    i.e. drop broken nocheck annotations
  * add metadata about upstream project
  * update git-buildpackage config:
    + filter out debian subdir
    + simplify usage comments
  * update watch file:
    + improve filename mangling
    + use Github API
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 06 Feb 2025 12:30:25 +0100

rust-async-fs (2.1.2-4) unstable; urgency=medium

  * autopkgtest-depend on dh-rust (not dh-cargo)
  * drop patches 2002_*, obsoleted by Debian package changes
  * tighten (build-)dependencies for crates async-lock futures-lite

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 17 Aug 2024 08:23:52 +0200

rust-async-fs (2.1.2-3) unstable; urgency=medium

  * simplify packaging;
    build-depend on dh-sequence-rust
    (not dh-cargo libstring-shellquote-perl)

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 12 Jul 2024 02:29:48 +0200

rust-async-fs (2.1.2-2) unstable; urgency=medium

  * no-change release targeted unstable

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 23 Jun 2024 08:59:53 +0200

rust-async-fs (2.1.2-1) experimental; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * bump project versions in virtual packages and autopkgtests
  * unfuzz patch 2001;
    add patches 2002_*
    to accept older branches of crates async-lock futures-lite;
    relax (build-)dependencies for crates async-lock futures-lite
  * stop (build-)depend on package for crate autopkg
  * relax to build- and autopkgtest-depend unversioned
    when version is satisfied in Debian stable

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 23 Jun 2024 08:54:43 +0200

rust-async-fs (1.6.0-5) unstable; urgency=medium

  * update dh-cargo fork
  * update copyright info: update coverage
  * declare compliance with Debian Policy 4.7.0

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 23 Jun 2024 08:40:09 +0200

rust-async-fs (1.6.0-4) unstable; urgency=medium

  * update dh-cargo fork;
    closes: bug#1047928, thanks to Lucas Nussbaum
  * update DEP-3 patch headers

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 14 Aug 2023 12:48:43 +0200

rust-async-fs (1.6.0-3) unstable; urgency=medium

  * tighten autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 02 Feb 2023 17:30:20 +0100

rust-async-fs (1.6.0-2) unstable; urgency=medium

  * change binary library package to be arch-independent
  * stop superfluously provide
    virtual unversioned or major-versioned feature packages
  * declare compliance with Debian Policy 4.6.2
  * update dh-cargo fork;
    build-depend on libstring-shellquote-perl
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 01 Feb 2023 15:02:30 +0100

rust-async-fs (1.6.0-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * bump version for provided virtual packages and autopkgtest hints
  * unfuzz patches
  * (build-)depend on librust-autocfg-1+default-dev

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 26 Aug 2022 08:28:45 +0200

rust-async-fs (1.5.0-2) unstable; urgency=medium

  * renumber patch 1001 -> 2001
    to match patch naming micro-policy
    (and rename to be more generic while at it)

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 12 Jul 2022 12:02:46 +0200

rust-async-fs (1.5.0-1) unstable; urgency=low

  * initial Release;
    closes: Bug#1014227

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 02 Jul 2022 21:11:36 +0200
